﻿#include <Windows.h>
#include <tchar.h>
#include <iostream>
#include <string>
#include <tlhelp32.h>

namespace
{	
	void PrintLastError()
	{
		DWORD dwError = GetLastError();
		HANDLE hlocal = LocalAlloc(LMEM_FIXED, 64 * 1024);
		if (hlocal == NULL)
		{
			std::cout << "**Error of memory allocation  for an error message buffer is occurred!" << std::endl;
			return;
		}
		
		if (FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, dwError, MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US), (LPWSTR)hlocal, 64 * 1024, NULL))
		{
			std::wcout << dwError << ": " << (wchar_t*)hlocal << std::endl;
		}
		else
		{
			std::cout << "**Error of FormatMessage() execution is occured!" << std::endl;
		}
	}

	bool GetProcessId(LPCTSTR pname, DWORD* pid)
	{
		bool found = false;
		PROCESSENTRY32 peProcessEntry;
		const HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
		if (hSnapshot == INVALID_HANDLE_VALUE)
		{
			return false;
		}
		peProcessEntry.dwSize = sizeof(PROCESSENTRY32);
		Process32First(hSnapshot, &peProcessEntry);
		do
		{
			if (lstrcmpW(pname, peProcessEntry.szExeFile) != 0)
			{
				continue;
			}
			found = true;
			*pid = peProcessEntry.th32ProcessID;
			break;
		} while (Process32Next(hSnapshot, &peProcessEntry));

		CloseHandle(hSnapshot);

		return found;
	}

	bool InjectRThread(DWORD dwProcessId, LPCTSTR dllPath)
	{
		PTHREAD_START_ROUTINE pfnThreadRtn = (PTHREAD_START_ROUTINE)GetProcAddress(GetModuleHandle(TEXT("Kernel32")), "LoadLibraryW");

		HANDLE hProcessRemote = OpenProcess(PROCESS_CREATE_THREAD | // для CreateRemoteThread
			PROCESS_VM_OPERATION | // для VirtualAllocEx/VirtualFreeEx
			PROCESS_VM_WRITE, // для WriteProcessMemory
			FALSE, dwProcessId);
		if (hProcessRemote == NULL)
		{
			PrintLastError();
			return false;
		}
		int dllPathLen = (lstrlenW(dllPath) + 1) * sizeof(WCHAR);
		PWSTR pszLibFileRemote  = (PWSTR)VirtualAllocEx(hProcessRemote, NULL, dllPathLen, MEM_COMMIT, PAGE_READWRITE);		
		if (pszLibFileRemote == NULL)
		{
			PrintLastError();
			return false;
		}
		
		if (!WriteProcessMemory(hProcessRemote, pszLibFileRemote, (PVOID)dllPath, dllPathLen, NULL))
		{
			PrintLastError();
			return false;
		}
		
		HANDLE hThread = CreateRemoteThread(hProcessRemote, NULL, 0, pfnThreadRtn, pszLibFileRemote, 0, NULL);
		if (hThread == NULL)
		{
			PrintLastError();
			return false;
		}

		WaitForSingleObject(hThread, INFINITE);

		return true;
	}
}

//void main(int argc, wchar_t** argv)
void wmain(int argc, wchar_t** argv)
{	if (argc < 3)	{		std::cout << "**Err: 2 arguments are requared:\ninjected_process_name.exe path_to_the_dll" << std::endl;		return;	}	DWORD dwProcessId;
	if (!GetProcessId(argv[1], &dwProcessId))
	{		std::cout << "**Err: process is not found" << std::endl;
		return;
	}
		
	WIN32_FIND_DATA FindFileData;
	HANDLE handle = FindFirstFile(argv[2], &FindFileData);
	if (handle == INVALID_HANDLE_VALUE)
	{
		std::cout << "**Err: dll is not found" << std::endl;
		return;
	}
	FindClose(handle);

	if (!InjectRThread(dwProcessId, argv[2]))
	{
		std::cout << "**Err: InjectRThread returns false." << std::endl;
		return;
	}
	
	std::cout << "Success!" << std::endl;
}